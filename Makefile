SHELL = /bin/bash
VERSION ?= $(shell git describe --tags --always --dirty)
TAG ?=$(subst +,-,$(VERSION))

ifneq (,$(wildcard ./.env))
    include .env
	export
endif

docker-build:
	echo $(VERSION)
	docker build -t $(REGISTRY)/$(ORG)/$(IMG):$(TAG) --build-arg VERSION=$(VERSION) -f docker/Dockerfile . 

docker-push:
	echo $(VERSION)
	docker push $(REGISTRY)/$(ORG)/$(IMG):$(TAG)

docker-push:
	docker push $(REGISTRY)/$(ORG)/$(IMG):$(TAG)
	
create-certificate:
	openssl req -x509 -sha256 -newkey rsa:2048 -keyout webhook.key -out webhook.crt -days 1024 -nodes -addext "subjectAltName = DNS.1:$(SERVICENAME).$(NAMESPACE).svc"
