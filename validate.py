"""
A simple validation hook for Kubernetes Admission Controller.

It validates that the pod has a configurable label set.
"""

import os

from bottle import Bottle, request

app = Bottle()

LABEL = os.environ['LABEL']
KEY = os.environ['ALLOW_KEY']

@app.post('/validate')
def webhook(*args, **kwargs):
    """The validating webhook endpoint"""
    print(args, kwargs)
    request_info = request.json
    uid = request_info["request"].get("uid")

    name = request_info["request"]["object"]["metadata"].get("name")
    kind = request_info["request"]["kind"]["kind"]
    labels = request_info["request"]["object"]["metadata"].get("labels")

    if labels and labels.get(LABEL) == KEY:
        print(f'Object {kind}/{name} contains the required {LABEL} label. Allowing the request.')
        allowed, message = True, f"Label {LABEL} is set."

    else:
        print(f'Object {kind}/{name} doesn\'t have the required {LABEL} label. Request rejected!')
        allowed, message = False, f"Label {LABEL} is not set!"

    return {"apiVersion": "admission.k8s.io/v1",
                    "kind": "AdmissionReview",
                    "response":
                        {"allowed": allowed,
                         "uid": uid,
                         "status": {"message": message}
                         }
           }
