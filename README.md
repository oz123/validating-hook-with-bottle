Example of validating webhook for Kubernetes API using bottle.py
================================================================

This example is part of the Kubernetes Workshop for EuroPython 2023.

more examples at:


https://github.com/garethr/kubernetes-webhook-examples

https://github.com/prit342/simple-k8s-validating-webhook

CERT CREATION:

https://gist.github.com/tirumaraiselvan/b7eb1831d25dd9d59a785c11bd46c84b
